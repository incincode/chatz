import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";


var config = {
    apiKey: "AIzaSyCHi0jOJ35cr8AKeMiKKR3gqTj02Ky7HUk",
    authDomain: "chatz-4f8f0.firebaseapp.com",
    databaseURL: "https://chatz-4f8f0.firebaseio.com",
    projectId: "chatz-4f8f0",
    storageBucket: "chatz-4f8f0.appspot.com",
    messagingSenderId: "868906905222"
};
firebase.initializeApp(config);

export default firebase;
